var assert = require('assert');
var PenelopePlugin = require('..');
var Plugin = require('origami-plugin');
var EventEmitter = require('events').EventEmitter;
var ConnectedEmitters = require('origami-connected-emitters');

function Plugin1() {
}

Plugin1.prototype.method1 = function (param1) {
  
};

describe('PenelopePlugin', function () {
  it('requires a plugin', function () {
    assert.throws(
      function () {
        new PenelopePlugin();
      },
      /plugin is required/
    );
  });
  
  it('accepts plugin', function () {
    var plugin = new Plugin(Plugin1);
    new PenelopePlugin(plugin);
  });
  
  it('returns function', function () {
    var plugin = new Plugin(Plugin1);
    var result = new PenelopePlugin(plugin);
    
    assert.equal('function', typeof(result));
  });
  
  describe('with plugin', function () {
    var target;
    
    beforeEach(function () {
      var plugin = new Plugin(Plugin1);
      
      target = new PenelopePlugin(plugin);
    });
    
    it('answers describe-methods', function (done) {
      var socket = new EventEmitter();
      
      target(socket, {});
      
      socket
      .emit(
        'describe-methods', 
        function (err, methods) {
          try {
            assert(!err);
            assert.deepEqual({ method1: ['param1'] }, methods);
            
            done();
          } catch (e) {
            done(e);
          }
        }
      );
    });
    
    it('invokes plugin method on api', function (done) {
      var socket = new EventEmitter();
      function Plugin2(contextParam1) {
        this.contextParam1 = contextParam1;
      }
      
      Plugin2.prototype.method2 = function (param1) {
        try {
          assert.equal('contextParam1', this.contextParam1);
          assert.equal('param1', param1);
          
          done();
        } catch (e) {
          done(e);
        }
      };
      
      var plugin = new Plugin(Plugin2);
      
      target = new PenelopePlugin(plugin);

      target(socket, {});
      
      socket
      .emit(
        'api',
        null,
        { contextParam1: 'contextParam1' },
        'method2',
        { param1: 'param1' },
        function () {}
      );
    });
    
    it('invokes api callback with method error', function (done) {
      var socket = new EventEmitter();
      function Plugin3() {
      }
      
      Plugin3.prototype.method3 = function () {
        throw new Error('my error');
      };
      
      var plugin = new Plugin(Plugin3);
      
      target = new PenelopePlugin(plugin);

      target(socket, {});
      
      socket
      .emit(
        'api',
        null,
        { },
        'method3',
        { },
        function (err) {
          try {
            assert.equal('my error', err);
            
            done();
          } catch (e) {
            done(e);
          }
        }
      );
    });
  
    it('invokes api callback with method result', function (done) {
      var socket = new EventEmitter();
      
      function Plugin4() {
      }
      
      Plugin4.prototype.method4 = function () {
        return 'my result';
      };
      
      var plugin = new Plugin(Plugin4);
      
      target = new PenelopePlugin(plugin);
  
      target(socket, {});
      
      socket
      .emit(
        'api',
        null,
        { },
        'method4',
        { },
        function (err, result) {
          try {
            assert(!err);
            assert.equal('my result', result);
            
            done();
          } catch (e) {
            done(e);
          }
        }
      );
    });
    
    it('creates plugin instance with locals in the context', function (done) {
      var socket = new EventEmitter();
      
      function Plugin5(local1) {
        try {
          assert.equal(100, local1);
          
          done();
        } catch (e) {
          done(e);
        }
      }
      
      Plugin5.prototype.method5 = function () {};
      
      var plugin = new Plugin(Plugin5);
      
      target = new PenelopePlugin(plugin, { local1: 100});
  
      target(socket, {});
      
      socket
      .emit(
        'api',
        null,
        { },
        'method5',
        { },
        function () {
        }
      );
    });
    
    it('creates plugin instance with dependencies client', function (done) {
      function Plugin6(dep1) {
        try {
          assert(dep1);
          assert(dep1.method6dep1);
          
          done();
        } catch (e) {
          done(e);
        }
      }
      
      Plugin6.prototype.method6 = function () {};
      
      var plugin = new Plugin(Plugin6);
      
      target = new PenelopePlugin(plugin, {}, { 'dep1': 'Plugin6dep1' });
      
      var bus = new ConnectedEmitters();
      var socket1 = bus.createEmitter();
      var socket2 = bus.createEmitter();
  
      target(socket1, {});
      
      socket2
      .on(
        'describe-apis',
        function (callback) {
          callback(null, { 'Plugin6dep1': { method6dep1: [] }});
        }
      );
      
      socket2
      .emit(
        'api',
        null,
        { },
        'method6',
        { },
        function () {
        }
      );
    });
  });
});