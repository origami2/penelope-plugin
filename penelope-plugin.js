var Client = require('origami-client');

function plainError(obj) {
  if (obj instanceof Error) return obj.message;
  
  return obj;
}

function PenelopePlugin(plugin, locals, dependencies) {
  if (!plugin) throw new Error('plugin is required');
  locals = locals || {};
  
  return function (socket, params) {
    socket
    .on(
      'describe-methods',
      function (callback) {
        callback(null, plugin.describeMethods());
      }
    );
    
    socket
    .on(
      'api',
      function (
        stackToken,
        context,
        methodName,
        methodParams,
        callback
      ) {
        for (var localName in locals) {
          context[localName] = locals[localName];
        }
        
        if (!dependencies || Object.keys(dependencies).length === 0) {
          plugin
          .invokeMethod(stackToken, context, methodName, methodParams, callback)
          .then(function (result) {
            callback(null, result);
          })
          .catch(function (err) {
            callback(plainError(err));
          });
        } else {
          socket
          .emit(
            'describe-apis',
            function (err, apis) {
              if (err) return callback(err);
              
              var client = new Client(socket, apis, stackToken);
              
              for (var depName in dependencies) {
                context[depName] = client[dependencies[depName]];
              }
              
              plugin
              .invokeMethod(stackToken, context, methodName, methodParams, callback)
              .then(function (result) {
                callback(null, result);
              })
              .catch(function (err) {
                callback(plainError(err));
              });
            }
          );
        }
      }
    );
  };
}

module.exports = PenelopePlugin;